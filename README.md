# Traffic Signs Classification

A machine learning project, written in Python. Programmed a Convolutional Neural Network (CNN) to classify different types of traffic signs.

## Installation
The project was originally designed and written in the `Google Colaboratory IDE`, with `Google Drive` functioning as a mounted file system. 
For your convenience, I would strongly recommend working with the original copy of the project, which can be found at:
`https://colab.research.google.com/drive/1heqox9lCAMkYFESAHCbW1PtzhuSJuWq2`
(Access to the project will be permitted upon request).

However, if you choose to run the project on your local machine, please keep in mind that some adjustments will be required. The changes to the code are minor - mostly in the parts where `Google Drive` is mounted, and in the directories of the dataset. 

## Usage
To execute the project, simply run the code and follow along as the machine improves its accuracy of recognizing the different traffic signs.